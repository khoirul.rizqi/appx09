package rizqi.khoirul.appx09

import android.content.Context
import android.database.sqlite.SQLiteDatabase
import android.database.sqlite.SQLiteOpenHelper

class DBOpenHelper(context: Context):SQLiteOpenHelper(context,DB_Name,null,DB_Ver) {
    companion object{
        val DB_Name = "music"
        val DB_Ver = 1
    }

    override fun onCreate(db: SQLiteDatabase?) {
        val tMusik = "create table musik(id_music int primary key,id_cover int,music_title text)"
        val insT = " insert into musik values('0x7f0c0000','0x7f06005f','lagu_1'),('0x7f0c0001','0x7f060060','lagu_2'),('0x7f0c0002','0x7f060061','lagu_3')"
        db?.execSQL(tMusik)
        db?.execSQL(insT)
    }

    override fun onUpgrade(db: SQLiteDatabase?, oldVersion: Int, newVersion: Int) {

    }
}

//.field public static final lagu_1:I = 0x7f0c0000
//.field public static final lagu_2:I = 0x7f0c0001
//.field public static final lagu_3:I = 0x7f0c0002
//.field public static final vid_1:I = 0x7f0c0003
//.field public static final vid_2:I = 0x7f0c0004
//.field public static final vid_3:I = 0x7f0c0005
//.field public static final cover_1:I = 0x7f06005f
//.field public static final cover_2:I = 0x7f060060
//.field public static final cover_3:I = 0x7f060061