package rizqi.khoirul.appx09

import android.database.Cursor
import android.database.sqlite.SQLiteDatabase
import android.media.MediaPlayer
import android.net.Uri
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.Handler
import android.view.View
import android.widget.*
import kotlinx.android.synthetic.main.activity_main.*
import java.util.concurrent.TimeUnit

class MainActivity : AppCompatActivity(), View.OnClickListener, SeekBar.OnSeekBarChangeListener {
    var posLaguSkr = 0
    var posCoverSkr = 0
    var posJudulSkr = ""
    var handler = Handler()
    lateinit var mediaPlayer: MediaPlayer
    lateinit var mediaController: MediaController
    lateinit var db:SQLiteDatabase
    lateinit var adapter : ListAdapter
    var idMusik : Int = 0x7f0c0002
    var idCover : Int = 0x7f060061
    var idJudul : String = "lagu_3"
    var pMs : Int = idMusik
    var pCs : Int = idCover
    var pJs : String = idJudul
    var max : String = ""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        mediaController = MediaController(this)
        mediaPlayer = MediaPlayer()
        sbLagu.max = 100
        sbLagu.progress = 0
        sbLagu.setOnSeekBarChangeListener(this)
        btnNext.setOnClickListener(this)
        btnOff.setOnClickListener(this)
        btnPlay.setOnClickListener(this)
        btnPrev.setOnClickListener(this)
        db=DBOpenHelper(this).writableDatabase
        lvMusik.setOnItemClickListener(itemClicked)
    }

    val itemClicked = AdapterView.OnItemClickListener { parent, view, position, id ->
        val c : Cursor = parent.adapter.getItem(position) as Cursor
        idMusik = c.getInt(c.getColumnIndex("_id"))
        txJudul.setText(c.getString(c.getColumnIndex("judul")))
        idMusik = c.getInt(c.getColumnIndex("_id"))
        idCover = c.getInt(c.getColumnIndex("cover"))
        idJudul = c.getString(c.getColumnIndex("judul"))
        Toast.makeText(this,"Perubahan Telah Disimpan $idMusik, $idCover, $idJudul",Toast.LENGTH_SHORT).show()
        audioplaygo(idMusik,idCover,idJudul)
    }

    fun audioplaygo(a:Int,b:Int,c:String){
        mediaPlayer.stop()
        mediaPlayer = MediaPlayer.create(this,a)
        sbLagu.max = mediaPlayer.duration
        txJudul.setText(idJudul)
        txMax.setText(milliSecondToString(sbLagu.max))
        txCur.setText(milliSecondToString(mediaPlayer.currentPosition))
        imV.setImageResource(b)
        mediaPlayer.start()
        var updateSeekBarThread = UpdateSeekBarProgressThread()
        handler.postDelayed(updateSeekBarThread,50)
    }

    override fun onStart() {
        super.onStart()
        showdata()
        pl()
    }

    fun showdata(){
        var sql = "select id_music as _id, id_cover as cover,music_title as judul from musik"
        max = "select count(id_music) as i from musik"
        val c : Cursor = db.rawQuery(sql,null)
        adapter = SimpleCursorAdapter(this,R.layout.item_data_musik,c, arrayOf("_id","cover","judul"), intArrayOf(R.id.txIdMusik,R.id.txIdCover,R.id.txMusikTitle),CursorAdapter.FLAG_REGISTER_CONTENT_OBSERVER)
        lvMusik.adapter = adapter
    }

    fun pl(){
        posLaguSkr = pMs
        posCoverSkr = pCs
        posJudulSkr = pJs
    }

    fun milliSecondToString(ms : Int):String{
        var detik = TimeUnit.MILLISECONDS.toSeconds(ms.toLong())
        var menit = TimeUnit.SECONDS.toMinutes(detik)
        detik = detik % 60
        return "$menit : $detik"
    }

    fun audioNext(){
        if(mediaPlayer.isPlaying){
            mediaPlayer.stop()
        }
        if (posLaguSkr<0x7f0c0002){
            posLaguSkr++
            posCoverSkr++
            jud(posLaguSkr)

        }else{
            posLaguSkr = 0x7f0c0000
            posCoverSkr = 0x7f06005f
            jud(posLaguSkr)
        }
        audioplaygo(posLaguSkr,posCoverSkr,posJudulSkr)
    }

    fun jud(a:Int){
        if(a==0x7f0c0000){
            idJudul="lagu_1"

        }
        else if(a==0x7f0c0001){
            idJudul="lagu_2"
        }
        else{
            idJudul="lagu_3"
        }
    }

    fun audioPrev(){
        if(mediaPlayer.isPlaying){
            mediaPlayer.stop()
        }
        if(posLaguSkr>0x7f0c0000){
            posLaguSkr--
            posCoverSkr--
            jud(posLaguSkr)
        }else{
            posLaguSkr = 0x7f0c0002
            posCoverSkr = 0x7f060061
            jud(posLaguSkr)
        }
        audioplaygo(posLaguSkr,posCoverSkr,posJudulSkr)
    }

    fun audioStop(){
        if (mediaPlayer.isPlaying){
            mediaPlayer.stop()
        }
    }

    inner class UpdateSeekBarProgressThread : Runnable{
        override fun run() {
            var currTime = mediaPlayer.currentPosition
            txCur.setText(milliSecondToString(currTime))
            sbLagu.progress = currTime
            if (currTime != mediaPlayer.duration) {
                handler.postDelayed(this,50)
            }
        }
    }

    override fun onClick(v: View?) {
        when(v?.id){
            R.id.btnPlay->{
                audioplaygo(idMusik,idCover,idJudul)
            }
            R.id.btnOff->{
                audioStop()
            }
            R.id.btnNext->{
                audioNext()
            }
            R.id.btnPrev->{
                audioPrev()
            }
        }
    }

    override fun onProgressChanged(seekBar: SeekBar?, progress: Int, fromUser: Boolean) {
    }

    override fun onStartTrackingTouch(seekBar: SeekBar?) {
    }

    override fun onStopTrackingTouch(seekBar: SeekBar?) {
        seekBar?.progress?.let { mediaPlayer.seekTo(it) }
    }
}